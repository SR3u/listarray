//
//  listarray_decl.h
//  listarray
//
//  Created by Sergey Rump on 30.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#ifndef listarray_listarray_decl_h
#define listarray_listarray_decl_h

#include <iostream>
#define LOG(msg) std::clog<<__PRETTY_FUNCTION__<<" "<<__FILE__<<" "<<__LINE__<<"\n"<<msg<<"\n";
#include <vector>
namespace ttl//TemplateToolsLibrary
{
    template <typename value_t,size_t step>
    class ListArray
    {
        struct element_t
        {
            value_t value;
            element_t *next,*prev;
            element_t(value_t val):value(val){next=NULL;prev=NULL;}
            element_t(value_t val,element_t*_prev,element_t*_next):value(val)
            {
                next=_next;prev=_prev;
                _next->prev=this;_prev->next=this;
            }
            element_t(value_t val,element_t*_prev):value(val)
            {
                next=NULL;prev=_prev;_prev->next=this;
            }
        };
        typedef std::vector<element_t*> internal_container_t;
        element_t *_begin,*_end;
        internal_container_t ptrs;
        size_t count;
        element_t* ptr(size_t idx)
        {
            element_t*res=ptrs[idx/step];
            for (size_t i=idx%step;i>0;--i)
            {res=res->next;}
            return res;
        }
    public:
        class iterator: public std::iterator<std::random_access_iterator_tag,value_t>
        {
            size_t pos;
        public:
            typedef ptrdiff_t difference_type; //almost always ptrdif_t
            typedef value_t value_type; //almost always T
            typedef value_t& reference; //almost always T& or const T&
            typedef value_t* pointer; //almost always T* or const T*
            element_t *cur;
            iterator(){cur=NULL;}
            iterator(const iterator&i){cur=i.cur;pos=i.pos;}
            iterator(element_t *c){cur=c;}
            inline iterator operator=(const iterator &arg){pos=arg.pos;cur=arg.cur;return *this;}
            inline value_t& operator*(){return cur->value;}
            inline bool operator==(const iterator& par){return par.cur==this->cur;}
            inline bool operator!=(const iterator& par){return par.cur!=this->cur;}
            inline bool operator>=(const iterator& par){return pos>=par.pos;}
            inline bool operator<=(const iterator& par){return pos<=par.pos;}
            inline bool operator>(const iterator& par){return pos>par.pos;}
            inline bool operator<(const iterator& par){return pos<par.pos;}
            inline iterator& operator++(){pos++;cur=cur->next;return *this;}//prefix
            inline iterator& operator--(){pos--;cur=cur->prev;return *this;}
            inline iterator& operator++(int){pos++;cur=cur->next;return *this;}//postfix
            inline iterator& operator--(int){pos--;cur=cur->prev;return *this;}
            inline const iterator& operator+(const iterator &offset)
            {
                iterator i(*this);
                i=i+(this->pos-offset.pos);
                return i;
            }
            inline iterator operator+(size_t offset)
            {
                iterator _out(*this);
                _out+=offset;
                return _out;
            }
            inline friend difference_type operator-(iterator& a, iterator& b)
            {
                return a.pos-b.pos;
            }
            inline const iterator& operator-(const iterator& offset)
            {
                iterator i(*this);
                i=i-(this->pos-offset.pos);
                return i;
            }
            inline iterator operator-(size_t offset)
            {
                iterator _out(*this);
                _out-=offset;
                return _out;
            }
            inline iterator &operator+=(size_t offset)
            {
                offset+=pos;
                while (pos!=offset)
                {cur=cur->next;pos++;}
                return *this;
            }
            inline iterator &operator-=(size_t offset)
            {
                offset-=pos;
                while (pos!=offset)
                {cur=cur->prev;pos--;}
                return *this;
            }
        };
        iterator begin()const{return iterator(_begin);}
        iterator end()const{return iterator(NULL);}
        ListArray(void);
        ListArray(const ListArray &la);
        ~ListArray(void);
        inline ListArray& operator=(const ListArray &arg)
        {
            clear();
            for(iterator i=arg.begin(); i.cur!=NULL; i++)
                push_back(*i);
            return *this;
        }
        inline bool empty(){return count==0;}
        inline size_t capacity(){return count;}
        inline void clear();// remove all elements
        inline size_t size(){return count;}// returns number of elements
        inline void push_back(const value_t& val);
        inline void insert(size_t idx,const value_t& val);
        inline void erase(size_t idx);
        inline value_t& operator[](size_t idx);// get element #idx
    };
}


#endif
