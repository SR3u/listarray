//
//  listarray.h
//  listarray
//
//  Created by Sergey Rump on 30.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#ifndef __ListArray__ListArray__
#define __ListArray__ListArray__
#include "listarray_impl.cpp"
namespace ttl
{
    template<typename value_t> using container=ttl::ListArray<value_t,10>;
}
#endif /* defined(__ListArray__ListArray__) */
