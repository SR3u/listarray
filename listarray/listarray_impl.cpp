//
//  listarray.cpp
//  listarray
//
//  Created by Sergey Rump on 30.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#include "listarray_decl.h"
namespace ttl//TemplateToolsLibrary
{
    template <typename value_t,size_t step>
    ListArray<value_t,step>::ListArray(void){count=0;_begin=_end=NULL;}
    template <typename value_t,size_t step>
    ListArray<value_t,step>::~ListArray(void){clear();}
    template <typename value_t,size_t step>
    ListArray<value_t,step>::ListArray(const ListArray &la)
    {
        clear();
        for(iterator i=la.begin();i.cur!=NULL;i++)
            push_back(*i);
    }
    template <typename value_t,size_t step>
    void ListArray<value_t,step>::clear()
    {
        if(count==0){return;}
        while (count>0)
        {
            element_t*tmp=_begin;
            _begin=_begin->next;
            delete tmp;
            count--;
        }
    }
    template <typename value_t,size_t step>
    value_t& ListArray<value_t,step>::operator[](size_t idx)
    {
        return ptr(idx%count)->value;
    }
    template <typename value_t,size_t step>
    void ListArray<value_t,step>::push_back(const value_t& val)
    {
        if(_begin==NULL)
        {
            _begin=new element_t(val);
            count=1;
            _end=_begin;
            ptrs.push_back(_begin);
            return;
        }
        element_t *newel=new element_t(val,_end);
        _end=newel;
        count++;
        if(count%step==0){ptrs.push_back(_end);}
    }
    template <typename value_t,size_t step>
    void ListArray<value_t,step>::erase(size_t idx)
    {
        idx%=count;
        element_t* rm=ptr(idx);
        if(rm->prev!=NULL)
            rm->prev->next=rm->next;
        if(rm->next!=NULL)
        rm->next->prev=rm->prev;
        delete rm;
        //shifting ptrs one element right
        size_t i=idx/step;
        if(idx%step){++i;}
        for (;i<ptrs.size();i++)
        {
            ptrs[i]=ptrs[i]->next;
            if(ptrs[i]==NULL){ptrs.erase(ptrs.begin()+i);}
        }
        count--;
    }
    template <typename value_t,size_t step>
    void ListArray<value_t,step>::insert(size_t idx,const value_t& val)
    {
        idx%=count;
        if(idx>=count){push_back(val);return;}
        element_t* next=ptr(idx);
        new element_t(val,next->prev,next);
        //shifting ptrs one element left
        size_t i=idx/step;
        if(idx%step){++i;}
        for (;i<ptrs.size();i++){ptrs[i]=ptrs[i]->prev;}
        count++;
        if(count%step==0)
        {ptrs.push_back(_end);}
    }
}
