//
//  main.cpp
//  listarray
//
//  Created by Sergey Rump on 30.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//
#include <iostream>
#include "tests/performance.h"
#include "listarray/listarray.h"
#include <algorithm>
bool compare_int(int a,int b){return a<b;}//compare for std::sort
void sort_test()
{
    ttl::container<int>a;
    for(size_t i=0; i<35; i++)
        a.push_back(rand()%255);
    std::cout<<"before std::sort:\na: ";
    for (ttl::container<int>::iterator i=a.begin(); i!=a.end(); i++)
        std::cout<<*i<<" ";
    std::sort(a.begin(),a.end(),compare_int);//std::sort
    std::cout<<"after std::sort:\n";
    std::cout<<"a: ";
    for (ttl::container<int>::iterator i=a.begin(); i!=a.end(); i++)
    std::cout<<*i<<" ";
    std::cout<<'\n';
}
int main(int argc, const char * argv[])
{    
    performance_tests();
    sort_test();
    return 0;
}
