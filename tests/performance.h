//
//  performance.h
//  listarray
//
//  Created by Sergey Rump on 30.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#ifndef __listarray__performance__
#define __listarray__performance__

#if DEBUG
#define BUILD_TYPE "DEBUG"
#else
#define BUILD_TYPE "RELEASE"
#endif

void performance_tests();

#endif /* defined(__listarray__performance__) */
