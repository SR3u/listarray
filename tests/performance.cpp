//
//  performance.cpp
//  listarray
//
//  Created by Sergey Rump on 30.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#include "performance.h"
#include "listarray.h"
#include <list>
#include <chrono>
#define __STEP__ 10
struct _TYPE_
{
    int8_t data[48];
};
bool compare(_TYPE_ a,_TYPE_ b){return a.data[0]<b.data[0];}//compare for std::sort
#define ENABLE_DURATION std::chrono::high_resolution_clock::time_point begin;\
std::chrono::high_resolution_clock::time_point end;
#define DURATION(_varname_,_CODE_)\
begin = std::chrono::high_resolution_clock::now();\
{_CODE_}\
end = std::chrono::high_resolution_clock::now();\
auto _varname_ = std::chrono::duration_cast<std::chrono::microseconds>(end-begin).count();
typedef ttl::ListArray<_TYPE_,__STEP__>  ListArray;
void performance_test(size_t max_size,size_t max_iters)
{
    std::cout<<"Build: "<<BUILD_TYPE"\n";
    std::cout<<"sizeof(ttl::ListArray<float,10>): "<<sizeof(ttl::ListArray<float,10>)<<'\n';
    size_t *random_indices=new size_t[max_iters];
    for(size_t i=0;i<max_size;i++){random_indices[i]=random()%max_size;}
    _TYPE_ dummy;
    ListArray *l=new ListArray ();
    ENABLE_DURATION
    std::cout<<"ListArray<step="<<__STEP__<<"> vs std::vector and std::list on "<<max_size<<" elements of size "
            <<sizeof(_TYPE_)<<" bytes:"<<"\n";
    DURATION(listarray_add,
             for (size_t i=0; i<max_size; i++)
             {
                 l->push_back(dummy);
             });
    std::cout<<"listarray add time: "<<listarray_add<<"\n";
    DURATION(listarray_acess,
             for (size_t i=0; i<max_iters; i++)
             {
                 dummy=l->operator[](random_indices[i]);
             })
    std::cout<<"listarray acess time: "<<listarray_acess<<"\n";
    size_t pos=l->size()/2;
    DURATION(listarray_insert,
             for (size_t i=0; i<max_size; i++)
             {
                 l->insert(pos,dummy);
             })
    std::cout<<"listarray insert time: "<<listarray_insert<<"\n";
    DURATION(listarray_erase,
             for (size_t i=0; i<max_size; i++)
             {
                 l->erase(pos);
             })
    std::cout<<"listarray erase time: "<<listarray_erase<<"\n";
    ListArray la;
    DURATION(listarray_copy,la=*l;)
    std::cout<<"listarray copy time: "<<listarray_copy<<"\n";
    DURATION(listarray_delete,delete l;)
    std::cout<<"listarray delete time: "<<listarray_delete<<"\n";
    std::vector<_TYPE_>*v=new std::vector<_TYPE_>();
    DURATION(vector_add,
             for (size_t i=0; i<max_size; i++)
             {
                 v->push_back(dummy);
             })
    std::cout<<"vector add time: "<<vector_add<<"\n";
    DURATION(vector_acess,
             for (size_t i=0; i<max_iters; i++)
             {
                 dummy=(v->operator[](random_indices[i]));
             })
    std::cout<<"vector acess time: "<<vector_acess<<"\n";
    pos=v->size()/2;
    DURATION(vector_insert,
             for (size_t i=0; i<max_size; i++)
             {
                 v->insert(v->begin()+pos,dummy);
             })
    std::cout<<"vector insert time: "<<vector_insert<<"\n";
    DURATION(vector_erase,
             for (size_t i=0; i<max_size; i++)
             {
                 v->erase(v->begin()+pos);
             })
    std::cout<<"vector erase time: "<<vector_erase<<"\n";
    std::vector<_TYPE_>va;
    DURATION(vector_copy,va=*v;)
    std::cout<<"vector copy time: "<<vector_copy<<"\n";    
    DURATION(vector_delete,delete v;)
    std::cout<<"vector delete time: "<<vector_delete<<"\n";
    std::list<_TYPE_>*ls=new std::list<_TYPE_>();
    DURATION(list_add,
             for (size_t i=0; i<max_size; i++)
             {
                 ls->push_back(dummy);
             })
    std::cout<<"list add time: "<<list_add<<"\n";
    DURATION(list_acess,
             for (size_t i=0; i<max_iters; i++)
             {
                 dummy=(*std::next(ls->begin(), random_indices[i]));
             })
    std::cout<<"list acess time: "<<list_acess<<"\n";
    pos=v->size()/2;
    DURATION(list_insert,
             for (size_t i=0; i<max_size; i++)
             {
                 ls->insert(std::next(ls->begin(),pos),dummy);
             })
    std::cout<<"list insert time: "<<list_insert<<"\n";
    DURATION(list_erase,
             for (size_t i=0; i<max_size; i++)
             {
                 ls->erase(std::next(ls->begin(),pos));
             })
    std::cout<<"list erase time: "<<list_erase<<"\n";
    std::list<_TYPE_>lsa;
    DURATION(list_copy,lsa=*ls;)
    std::cout<<"vector copy time: "<<list_copy<<"\n";
    DURATION(list_delete,delete ls;)
    std::cout<<"list delete time: "<<list_delete<<"\n";
    std::cout<<"***************************************\n";
    std::cout<<"*******listarray vs std::vector********\n";
    std::cout<<"listarray_add/vector_add = "<<(long double)listarray_add/vector_add<<"\n";
    std::cout<<"listarray_acess/vector_acess = "<<(long double)listarray_acess/vector_acess<<"\n";
    std::cout<<"listarray_delete/vector_delete = "<<(long double)listarray_delete/vector_delete<<"\n";
    std::cout<<"listarray_insert/vector_insert = "<<(long double)listarray_insert/vector_insert<<"\n";
    std::cout<<"listarray_erase/vector_erase = "<<(long double)listarray_erase/vector_erase<<"\n";
    std::cout<<"listarray_copy/vector_copy = "<<(long double)listarray_copy/vector_copy<<"\n";
    std::cout<<"********listarray vs std::list*********\n";
    std::cout<<"listarray_add/list_add = "<<(long double)listarray_add/list_add<<"\n";
    std::cout<<"listarray_acess/list_acess = "<<(long double)listarray_acess/list_acess<<"\n";
    std::cout<<"listarray_delete/list_delete = "<<(long double)listarray_delete/list_delete<<"\n";
    std::cout<<"listarray_insert/list_insert = "<<(long double)listarray_insert/list_insert<<"\n";
    std::cout<<"listarray_erase/list_erase = "<<(long double)listarray_erase/list_erase<<"\n";
    std::cout<<"listarray_copy/list_copy = "<<(long double)listarray_copy/list_copy<<"\n";
    std::cout<<"*******std::vector vs std::list********\n";
    std::cout<<"vector_add/list_add = "<<(long double)vector_add/list_add<<"\n";
    std::cout<<"vector_acess/list_acess = "<<(long double)vector_acess/list_acess<<"\n";
    std::cout<<"vector_delete/list_delete = "<<(long double)vector_delete/list_delete<<"\n";
    std::cout<<"vector_insert/list_insert = "<<(long double)vector_insert/list_insert<<"\n";
    std::cout<<"vector_erase/list_erase = "<<(long double)vector_erase/list_erase<<"\n";
    std::cout<<"vector_copy/list_copy = "<<(long double)vector_copy/list_copy<<"\n";
    std::cout<<"***************************************\n";
}

void performance_tests()
{
    performance_test(10000,10000);
}